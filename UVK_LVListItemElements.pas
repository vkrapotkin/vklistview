unit UVK_LVListItemElements;

interface
uses
  FMX.Objects,
  FMX.Graphics,
  FMX.ListView.Types,
  FMX.Effects,
  system.UITypes,
  system.Types,
  system.Math.Vectors,
  Generics.Collections,
  UVKShadowGenerator;

type
  TVKListItemEllipse = class(TListItemDrawable)
  private
    FColor: TAlphaColor;
    FBorderColor: TAlphaColor;
    FLineWidth: Single;
    FCircle: Boolean;
    procedure SetColor(const Value: TAlphaColor);
    procedure SetBorderColor(const Value: TAlphaColor);
    procedure SetLineWidth(const Value: Single);
    procedure SetCircle(const Value: Boolean);
  public

    constructor Create(const AOwner: TListItem); override;
    procedure Render( const Canvas:TCanvas; const DrawItemindex: Integer;
                      const DrawStates: TListItemDrawStates;
                     const Resources: TListItemStyleResources; const Params: FMX.ListView.Types.TListItemDrawable.TParams;
                      const SubPassNo: Integer=0); override;
    property Color:TAlphaColor read FColor write SetColor;
    property BorderColor:TAlphaColor read FBorderColor write SetBorderColor;
    property LineWidth:Single read FLineWidth write SetLineWidth;
    property Circle:Boolean read FCircle write SetCircle;
  end;

  TVKListItemCorner = class(TListItemDrawable)
  private
    FColor: TAlphaColor;
    FSize: integer;
    procedure SetColor(const Value: TAlphaColor);
    procedure SetSize(const Value: integer);
  public
    procedure Render( const Canvas:TCanvas; const DrawItemindex: Integer;
                      const DrawStates: TListItemDrawStates;
                     const Resources: TListItemStyleResources; const Params: FMX.ListView.Types.TListItemDrawable.TParams;
                      const SubPassNo: Integer=0); override;
    property Color:TAlphaColor read FColor write SetColor;
    property Size: integer read FSize write SetSize;
  end;

  TVKShadowProps = record
  public
    distance: integer;
    enabled: Boolean;
  end;

  TVKListItemRect = class(TListItemDrawable)
  private
    FColor: TAlphaColor;
    FBorderColor: TAlphaColor;
    FLineWidth: Single;
    FSelectedColor: TAlphaColor;
    FSelectedBorderColor: TAlphaColor;
    FShadowProps: TVKShadowProps;
    FXRadius: Integer;
    FYRadius: Integer;
    FShadow: TVKShadowProps;
    procedure SetColor(const Value: TAlphaColor);
    procedure SetBorderColor(const Value: TAlphaColor);
    procedure SetLineWidth(const Value: Single);
    procedure SetSelectedBorderColor(const Value: TAlphaColor);
    procedure SetSelectedColor(const Value: TAlphaColor);
    procedure SetXRadius(const Value: Integer);
    procedure SetYRadius(const Value: Integer);
    procedure SetShadow(const Value: TVKShadowProps);
  public
    shdGen:TVKshadowGenerator;
    constructor Create(const AOwner: TListItem); override;
    destructor Destroy; override;

    procedure Render( const Canvas:TCanvas; const DrawItemindex: Integer;
                      const DrawStates: TListItemDrawStates;
                     const Resources: TListItemStyleResources; const Params: FMX.ListView.Types.TListItemDrawable.TParams;
                      const SubPassNo: Integer=0); override;
    property Color:TAlphaColor read FColor write SetColor;
    property BorderColor:TAlphaColor read FBorderColor write SetBorderColor;
    property LineWidth:Single read FLineWidth write SetLineWidth;
    property SelectedColor:TAlphaColor read FSelectedColor write SetSelectedColor;
    property SelectedBorderColor:TAlphaColor read FSelectedBorderColor write SetSelectedBorderColor;
    property XRadius: Integer read FXRadius write SetXRadius;
    property YRadius: Integer read FYRadius write SetYRadius;
    property Shadow: TVKShadowProps read FShadow write SetShadow;
  end;

  TVKListItemLine = class(TListItemDrawable)
  private
    FColor: TAlphaColor;
    FLineWidth: Single;
    procedure SetColor(const Value: TAlphaColor);
    procedure SetLineWidth(const Value: Single);
  public
    constructor Create(const AOwner: TListItem); override;
    procedure Render( const Canvas:TCanvas; const DrawItemindex: Integer;
                      const DrawStates: TListItemDrawStates;
                     const Resources: TListItemStyleResources; const Params: FMX.ListView.Types.TListItemDrawable.TParams;
                      const SubPassNo: Integer=0); override;
    property Color:TAlphaColor read FColor write SetColor;
    property LineWidth:Single read FLineWidth write SetLineWidth;
  end;

implementation

uses
  FMX.Types, system.sysutils, system.Math;

{ TListItemCircleText }


constructor TVKListItemEllipse.Create(const AOwner: TListItem);
begin
  inherited;
  FLineWidth := 1;
end;

procedure TVKListItemEllipse.Render(const Canvas: TCanvas;
  const DrawItemindex: Integer; const DrawStates: TListItemDrawStates;
  const Resources: TListItemStyleResources; const Params: FMX.ListView.Types.TListItemDrawable.TParams;
  const SubPassNo: Integer);
var
  r:TRectF;
begin
  if SubPassNo<>0 then
    exit;

  r := LocalRect;
  if Circle then
  begin
    if r.Width>r.Height then
      r.Inflate((r.Height-r.Width)/2,0)
    else if r.Width<r.Height then
      r.Inflate(0, (r.Width-r.Height)/2);
  end;
  Canvas.Fill.Color := Color;
  Canvas.FillEllipse(r,1);
  Canvas.Stroke.Color := BorderColor;
  Canvas.Stroke.Kind := TBrushKind.Solid;
  Canvas.Stroke.Thickness := LineWidth;
  Canvas.DrawEllipse(r,1);
end;


procedure TVKListItemEllipse.SetBorderColor(const Value: TAlphaColor);
begin
  FBorderColor := Value;
  Invalidate;
end;

procedure TVKListItemEllipse.SetCircle(const Value: Boolean);
begin
  FCircle := Value;
  Invalidate;
end;

procedure TVKListItemEllipse.SetColor(const Value: TAlphaColor);
begin
  FColor := Value;
  Invalidate;
end;

procedure TVKListItemEllipse.SetLineWidth(const Value: Single);
begin
  FLineWidth := Value;
  Invalidate;
end;

{ TListItemCorner }

procedure TVKListItemCorner.Render(const Canvas: TCanvas;
  const DrawItemindex: Integer; const DrawStates: TListItemDrawStates;
  const Resources: TListItemStyleResources; const Params: FMX.ListView.Types.TListItemDrawable.TParams;
  const SubPassNo: Integer);
var pts:TPolygon;
begin
  if SubPassNo<>0 then
    exit;
  SetLength(pts,4);
  Canvas.Fill.Color := Color;
  Canvas.Fill.Kind := TBrushKind.Solid;
  pts[0] := LocalRect.BottomRight;
  pts[1] := LocalRect.BottomRight;
  pts[2] := LocalRect.BottomRight;
  pts[3] := pts[0];
  pts[1].Offset(0,-size);
  pts[2].Offset(-size,0);
  Canvas.FillPolygon(pts,1);
//  Canvas.FillRect(LocalRect,0,0,[], 1);
end;

procedure TVKListItemCorner.SetColor(const Value: TAlphaColor);
begin
  FColor := Value;
  Invalidate;
end;

procedure TVKListItemCorner.SetSize(const Value: integer);
begin
  FSize := Value;
  Invalidate;
end;

{ TListItemRect }

constructor TVKListItemRect.Create(const AOwner: TListItem);
begin
  inherited;
  FLineWidth := 1;
  shdGen := TVKshadowGenerator.Create();
end;

destructor TVKListItemRect.Destroy;
begin
  FreeAndNil(shdGen);
  inherited;
end;

procedure TVKListItemRect.Render(const Canvas: TCanvas;
  const DrawItemindex: Integer; const DrawStates: TListItemDrawStates;
  const Resources: TListItemStyleResources; const Params: FMX.ListView.Types.TListItemDrawable.TParams;
  const SubPassNo: Integer);
var r, rExt, LR, LRExt :TRectF;

  procedure DoDrawRect(cnv : TCanvas);
  begin
    cnv.BeginScene();
//    shdGen.srcBitmap.Canvas.Clear(0);
    if TListItemDrawState.Selected in DrawStates then
      Cnv.Fill.Color := SelectedColor
    else
      Cnv.Fill.Color := Color;
    Cnv.FillRect(r,XRadius,YRadius, AllCorners,1);
    if TListItemDrawState.Selected in DrawStates then
      Cnv.Stroke.Color := SelectedBorderColor
    else
      Cnv.Stroke.Color := BorderColor;
    Cnv.Stroke.Kind := TBrushKind.Solid;
    Cnv.Stroke.Thickness := LineWidth;
    Cnv.DrawRect(r,XRadius,YRadius, AllCorners, 1);
    cnv.EndScene;
  end;

begin
  if SubPassNo<>0 then
    exit;

  R := LocalRect;
  if Shadow.enabled then
  begin
    LR := LocalRect;
    LRExt := LR;
    LRExt.inflate(20,20);
    R := TRectF.Create(PointF(20, 20), LR.Width, LR.Height);
    RExt := TRectF.Create(PointF(0, 0), LR.Width+40, LR.Height+40);

    shdGen.srcBitmap.SetSize(RExt.Size.Ceiling);
    DoDrawRect(shdGen.srcBitmap.Canvas);
    shdGen.CreateShadow(Shadow.distance);

    Canvas.BeginScene();
    Canvas.DrawBitmap(shdGen.resBitmap, shdGen.resBitmap.BoundsF, LRExt, 1);

    // drawing contol contour for debug
    {
    Canvas.Stroke.Color := TAlphaColorRec.red;
    Canvas.Stroke.Kind := TBrushKind.Solid;
    Canvas.Stroke.Thickness := 2;
    Canvas.DrawRect(localrect, XRadius, YRadius, AllCorners, 1);
    }

    Canvas.EndScene;
  end
  else
    DoDrawRect(Canvas);

  //shdGen.resBitmap.SaveToFile( ExtractFilePath(paramstr(0))+ 't'+ceil(LR.Left).ToString+'-'+ceil(LR.top).ToString+'.png' );

end;

procedure TVKListItemRect.SetBorderColor(const Value: TAlphaColor);
begin
  FBorderColor:=Value;
  Invalidate;
end;

procedure TVKListItemRect.SetColor(const Value: TAlphaColor);
begin
  FColor:=Value;
  Invalidate;
end;

procedure TVKListItemRect.SetLineWidth(const Value: Single);
begin
  FLineWidth := Value;
  Invalidate;
end;

procedure TVKListItemRect.SetSelectedBorderColor(const Value: TAlphaColor);
begin
  FSelectedBorderColor := Value;
  Invalidate;
end;

procedure TVKListItemRect.SetSelectedColor(const Value: TAlphaColor);
begin
  FSelectedColor := Value;
  Invalidate;
end;

procedure TVKListItemRect.SetShadow(const Value: TVKShadowProps);
begin
  FShadow := Value;
  Invalidate;
end;

procedure TVKListItemRect.SetXRadius(const Value: Integer);
begin
  FXRadius := Value;
  Invalidate;
end;

procedure TVKListItemRect.SetYRadius(const Value: Integer);
begin
  FYRadius := Value;
  Invalidate;
end;

{ TListItemList }

constructor TVKListItemLine.Create(const AOwner: TListItem);
begin
  inherited;
  FLineWidth := 1;
end;

procedure TVKListItemLine.Render(const Canvas: TCanvas;
  const DrawItemindex: Integer; const DrawStates: TListItemDrawStates;
  const Resources: TListItemStyleResources;
  const Params: FMX.ListView.Types.TListItemDrawable.TParams;
  const SubPassNo: Integer);
var
  p1,p2:TPointF;
begin
  if SubPassNo<>0 then
    exit;

  Canvas.Stroke.Color := Color;
  Canvas.Stroke.Kind := TBrushKind.Solid;
  p1:=LocalRect.TopLeft;
  p2:=LocalRect.BottomRight;
  Canvas.Stroke.Thickness := LineWidth;
  Canvas.DrawLine(p1, p2, 1);
end;

procedure TVKListItemLine.SetColor(const Value: TAlphaColor);
begin
  FColor := Value;
  Invalidate;
end;

procedure TVKListItemLine.SetLineWidth(const Value: Single);
begin
  FLineWidth := Value;
  Invalidate;
end;


end.

