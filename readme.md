﻿A little framework based on a standard TListView.
Features added:

Json-patterns to set ListViewItem's layout.

Several additional ListView elements inherited from TDrawable to use along with standard ones.

Adapter to fill ListView from collection.

Auto-sized item height.

All elements sizes can be described as math expressions like "ItemWidth / 2 - PriceText.W + GAP"


Uses external packages

https://bitbucket.org/vkrapotkin/jsonableobject/src/master

https://bitbucket.org/vkrapotkin/vkexpressionparser/src/master

https://bitbucket.org/vkrapotkin/vkshadowgenerator/src/master

https://github.com/vkrapotkin/x-superobject

Usage:
See Demo folder.