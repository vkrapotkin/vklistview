unit UDataModel;

interface
uses
  System.Types, System.Sysutils, System.Generics.Collections,
  XSuperObject,
  JsonableObject;

type
  TMyData=class
  public
    id: Integer;
    Text:string;
    Detail:string;
    Balance:Currency;
    Reserved:Currency;
    img:string;
    Status:integer;
    procedure AssignFrom(const obj:TMyData);
    function Clone():TMydata;
  end;

  TMyDataList=class(TJsonableObject)
  private
  public
    [DISABLE]
    Items: TObjectList<TMyData>;
    [Alias('Items')]
    _Items: TArray<TMyData>;

    constructor Create; override;
    destructor Destroy; override;

    procedure AfterLoad(X:ISuperObject); override;
  end;

function ExePath:string;

implementation
uses
  System.IOUtils;

function ExePath:string;
begin
  {$IFDEF MSWINDOWS}
  Result := ExtractFilePath(ParamStr(0));
  {$ENDIF}
  {$IFDEF ANDROID}
  result := IncludeTrailingBackslash(system.ioutils.TPath.GetDocumentsPath);
  {$ENDIF}
end;

{ TMyDataList }

procedure TMyDataList.AfterLoad(X:ISuperObject);
begin
  ArrayToList<TMyData>(_Items, items);
end;

constructor TMyDataList.Create;
begin
  inherited Create;
  Items := TObjectList<TMyData>.Create(True);
end;

destructor TMyDataList.Destroy;
begin
  FreeAndNil(Items);
  inherited;
end;

{ TMyData }

procedure TMyData.AssignFrom(const obj: TMyData);
begin
  id:=obj.id;
  Text:=obj.Text;
  Detail:=obj.Detail;
  Balance:=obj.Balance;
  Reserved:=obj.Reserved;
  img:=obj.img;
  Status:=obj.Status;
end;

function TMyData.Clone():TMyData;
begin
  result := TMyData.Create;
  Result.AssignFrom(self);
end;

end.
