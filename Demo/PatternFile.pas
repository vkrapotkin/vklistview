unit PatternFile;

interface

const
  pattern =
'{' + #13#10 +
  '"Columns":' + #13#10 +
  '[' + #13#10 +
  '' + #13#10 +
  '],' + #13#10 +
  '"Objects":' + #13#10 +
  '[' + #13#10 +
    '{"Name":"ID","Kind":"data"},' + #13#10 +
    '' + #13#10 +
    '{"Name":"Text","Kind":"text",' + #13#10 +
      '"Place":{"X":"10","Y":"5","W":"auto","H":"auto"},' + #13#10 +
      '"Color":"Brown",' + #13#10 +
      '"Font":{"Size":"16","Style":"bold"}},' + #13#10 +
      '' + #13#10 +
    '{"Name":"Detail","Kind":"text",' + #13#10 +
      '"Place":{"X":"20","Y":"Text.Bottom+5","W":"ItemWidth/2 - X","H":"auto","mutable":"H"},' + #13#10 +
      '"WordWrap":true,' + #13#10 +
      '"Font":{"Size":"12","Style":""}},' + #13#10 +
      '' + #13#10 +
    '{"Name":"BonusRect","Kind":"rect",' + #13#10 +
      '"Place":{"X":"itemwidth*3/4-10","Y":"Text.Y","W":"ItemWidth/4-5","H":"50"},' + #13#10 +
      '"BorderColor":"#FF005500", "Color":"lime",' + #13#10 +
       '"LineWidth":3, "Shadow": {"Enabled":false, "distance": 4} ' + #13#10 +
      '},' + #13#10 +
       '' + #13#10 +
    '{"Name":"img","Kind":"image",' + #13#10 +
     '"Place":{"X":"itemwidth/2","Y":"0","W":"48","H":"48"},' + #13#10 +
     '"HAlign":"leading","VAlign":"center",' + #13#10 +
    '},' + #13#10 +
    '' + #13#10 +
    '{"Name":"Balance","Kind":"text","TextHAlign":"trailing","TextVAlign":"center",' + #13#10 +
      '"Place":{"X":"BonusRect.x+5","Y":"Text.Y","W":"BonusRect.w-10","H":"50"},' + #13#10 +
      '"WordWrap":true,' + #13#10 +
      '"Color"	:"Black",' + #13#10 +
      '"Font":{"Size":"18","Style":""}}' + #13#10 +
  '],' + #13#10 +
  '"ItemHeight":"detail.bottom+10",' + #13#10 +
  '"ItemSpaces":    {"X":"","Y":"","W":"","H":""}' + #13#10 +
  '}';

implementation

end.
