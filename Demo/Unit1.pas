unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.ListView.Types, FMX.ListView.Appearances, FMX.ListView.Adapters.Base,
  FMX.ListView, FMX.Objects, System.ImageList, FMX.ImgList, FMX.TextLayout,
  System.Rtti,  FMX.Controls.Presentation, System.Generics.Collections,
  FMX.ScrollBox, FMX.StdCtrls, UVK_LVPatterns, UDataModel;

type
  TMyAdapter = class(TVK_LV_ListAdapter<TMyData>)
  public
    Data:TMyDataList;
    function List():TObjectList<TMyData>; override;
    procedure SetupDrawableContent(const AListViewItem: TListViewItem; const ADrawable: TListItemDrawable; const AData: TMyData); override;
    procedure SetItemData(const AListViewItem: TListViewItem; const AData:TMyData; const AName:string); override;
    procedure AfterLayout(const AListViewItem: TListViewItem); override;

    constructor Create(const AListView:TListView); override;
    destructor Destroy; override;
  end;

  TForm1 = class(TForm)
    lvWallets: TListView;
    r1: TRectangle;
    r2: TRectangle;
    lbl1: TText;
    b1: TSpeedButton;
    StyleBook1: TStyleBook;
    il1: TImageList;
    lbl2: TText;
    lbl3: TText;
    b2: TButton;
    procedure lvWalletsUpdatingObjects(const Sender: TObject;
      const AItem: TListViewItem; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure b1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lbl1Click(Sender: TObject);
    procedure b2Click(Sender: TObject);
    procedure lvWalletsResize(Sender: TObject);
  private
    Adapter : TMyAdapter;
    FTotal: Int64;
    FCount: int64;
    FAvg: single;
//    FUpdatinglvWalletsData:Boolean;
//    procedure UpdateWalletsList;
    procedure UpdateCounters;
  public
  end;

var
  Form1: TForm1;

implementation

uses
  UVK_LVListItemElements, FMX.Utils, XSuperObject, System.IOUtils, PatternFile ;

const
  sID = 'id';
  sText = 'Text';
  sDetail = 'Detail';
  sAmount = 'Amount';
  sAmountValue = 'AmountValue';
  sHold = 'Hold';
  sHoldValue = 'HoldValue';
  sStatusimage = 'StatusImage';
  sAccessory = 'Accessory';
  sRect = 'Rect';
  sLine = 'Line';

const
  img_DELETED=-1;
  img_ACTIVE=0;
  img_EMPTY=-1;

const
  sORDERS='orders';
  sBUILDINGS='buildings';
  sACTIONS='actions';
  sPHONES='phones';
  sBOOKMARKS='bookmarks';

  IMG_ORDERS=0;
  IMG_BUILDINGS=1;
  IMG_ACTIONS=2;
  IMG_PHONES=3;
  IMG_BOOKMARKS=4;



  EMERALD=$ff95f2f2;

JSON='{"Items":[{"id":1,"Text":"�������","Detail":"��������","Balance":250,"Reserved":0,"img":"'+sORDERS+'","Status":-1},' +
     '{"id":2,"Text":"��������","Detail":"��������������","Balance":100,"Reserved":53,"img":"'+sBUILDINGS+'","Status":-0},' +
     '{"id":3,"Text":"��������:","Detail":"��� ����������","Balance":0,"Reserved":0,"img":"'+sACTIONS+'","Status":-1},' +
     '{"id":4,"Text":"�������","Detail":"�����������","Balance":123,"Reserved":12,"img":"'+sPHONES+'","Status":0},' +
     '{"id":5,"Text":"��������","Detail":"��� ����� ��� ��� ����� ��� ��� ����� ��� ��� ����� ��� ��� ����� ��� ��� ����� ��� ���","Balance":465,"Reserved":11.11,"img":"'+sBOOKMARKS+'","Status":0},' +
     '{"id":6,"Text":"�������������","Detail":"�����������","Balance":0,"Reserved":0,"Status":0},' +
     '{"id":7,"Text":"������ ����","Detail":"�����������","Balance":14.55,"Reserved":0,"Status":0},' +
     '{"id":8,"Text":"����������","Detail":"�����������","Balance":7777.18,"Reserved":0,"Status":0}]}';

{$R *.fmx}

procedure TForm1.b2Click(Sender: TObject);
begin
  Adapter.ResetPlaces;
  width := 256 + Random(800);
  FCount := 0;
  FAvg:=0;
  FTotal:=0;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  i: Integer;
  j: Integer;
  obj:TMyData;
begin
  Adapter := TMyAdapter.Create(lvWallets);
  Adapter.data.LoadFromJSON(JSON);
//  we can use alternative way - load JSON from file
//  Adapter.data.LoadFromFile(ExePath()+'data.json',TEncoding.UTF8);
  for i := 0 to 199 do
  for j := 0 to 7 do
  begin
    obj:=Adapter.Data.Items[j].Clone();
    obj.id := 8 + 8*i + j;
    Adapter.Data.Items.Add(obj);
  end;
  Adapter.AddPatternFromJSON(pattern);
  Adapter.SetupListView();
  Adapter.ResetView();
  caption := 'count = '+inttostr(adapter.data.items.count);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FreeAndNil(Adapter);
end;

procedure TForm1.lbl1Click(Sender: TObject);
begin
  FCount:=0;
  updateCounters;
end;

procedure TForm1.lvWalletsResize(Sender: TObject);
begin
  if Adapter<>NIL then
    Adapter.ResetPlaces;
end;

procedure TForm1.lvWalletsUpdatingObjects(const Sender: TObject;
  const AItem: TListViewItem; var AHandled: Boolean);
var
  t:Int64;
begin
  t:=TThread.GetTickCount;
  Adapter.SetupContent(AItem);
  Adapter.DoLayout(AItem);
  Adapter.AfterLayout(AItem);
  AHandled := true;

  inc(FCount);
  Inc(FTotal,TThread.GetTickCount - t);
  FAvg := FTotal / FCount;
  updateCounters;
end;


procedure TForm1.UpdateCounters;
begin
  lbl1.Text := 'Count: '+FCount.ToString;
  lbl2.text := 'Average, msec: '+ FormatFloat('0.0',FAvg);
  lbl3.text := 'Total time, msec: '+FTotal.toString;
end;

procedure TForm1.b1Click(Sender: TObject);
begin
  if lvWallets.StyleLookup='lvWalletsStyle1' then
    lvWallets.StyleLookup:='lvWalletsStyle2'
  else
    lvWallets.StyleLookup:='lvWalletsStyle1';
  Adapter.ResetView;
end;


{ TMyAdapter }

procedure TMyAdapter.AfterLayout(const AListViewItem: TListViewItem);
var
  aData : TMyData;
  t:TListItemText;
begin
  aData := GetDataByListItem(AListViewItem);
  if (aData = nil) then
    exit;
  t := AListViewItem.Objects.FindObjectT<TListItemText>('Text');
  if t = nil then
    exit;
  if aData.id>100 then
    t.TextColor := TAlphaColorRec.Wheat
  else
    t.TextColor := TAlphaColorRec.Green;
end;

constructor TMyAdapter.Create(const AListView: TListView);
begin
  inherited;
  Data:=TMyDataList.Create();
end;

destructor TMyAdapter.Destroy;
begin
  FreeAndNil(Data);
  inherited;
end;

function TMyAdapter.List: TObjectList<TMyData>;
begin
  Result := data.Items;
end;

procedure TMyAdapter.SetItemData(const AListViewItem: TListViewItem;
  const AData: TMyData; const AName: string);
begin
  if AnsiSameText(AName,'ID') then
    AListViewItem.Data[AName] := AData.id;
end;

procedure TMyAdapter.SetupDrawableContent(const AListViewItem: TListViewItem; const ADrawable: TListItemDrawable; const AData: TMyData);
var
  sz:TSizeF;
begin
  if AnsiSameText( ADrawable.Name, 'text') then
  begin
    (ADrawable as TListItemText).Text := AData.Text;
  end
  else if AnsiSameText( ADrawable.Name, 'detail') then
  begin
    (ADrawable as TListItemText).Text := AData.Detail;
  end
  else if AnsiSameText( ADrawable.Name, 'balance') then
  begin
    (ADrawable as TListItemText).Text := FormatFloat('0.00', AData.Balance);
  end
  else if AnsiSameText( ADrawable.Name, 'reserved') then
  begin
    (ADrawable as TListItemText).Text := FormatFloat('0.00', AData.Reserved);
  end
  else if SameText( ADrawable.Name, 'img') then
  begin
    sz := TSizef.Create(48,48);
    if AnsiSameText(AData.img,sORDERS) then
//      (ADrawable as TListItemImage).ImageSource := TListItemImage.TImageSource.ImageList;
      (ADrawable as TListItemImage).ImageIndex := IMG_ORDERS
    else if AnsiSameText(AData.img,sBUILDINGS) then
      (ADrawable as TListItemImage).ImageIndex := IMG_BUILDINGS
    else if AnsiSameText(AData.img,sACTIONS) then
      (ADrawable as TListItemImage).ImageIndex := IMG_ACTIONS
    else if AnsiSameText(AData.img,sPHONES) then
      (ADrawable as TListItemImage).ImageIndex := IMG_PHONES
    else if AnsiSameText(AData.img,sBOOKMARKS) then
      (ADrawable as TListItemImage).ImageIndex := IMG_BOOKMARKS;
  end;
end;

end.
